package cn.tedu.tf_hotelmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TfHotelmanagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(TfHotelmanagerApplication.class, args);
    }

}
